FROM alpine:3.11.5

RUN apk add bash jq

COPY mo /usr/local/bin
COPY directory-listing /usr/local/bin

RUN chmod +x /usr/local/bin/mo
RUN chmod +x /usr/local/bin/directory-listing
