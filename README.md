## Usage in a pipeline

```
image: node:8

pipelines:
  tags:
    v*:
      - step:
          name: create directory listing
          image: <AWS IMAGE>
          artifacts:
            - json.json
          script:
            - aws s3api list-objects-v2 --bucket <BUCKET_NAME> --output json > json.json
      - step:
          name: create index.html page
          image: myrtle/s3-object-listing:latest
          artifacts:
            - index.html
            - json.json
          script:
            - directory-listing $(readlink -f ../json.json) > index.html
```
